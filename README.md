# README #

This is an annotation tool, that based on Princeton Xiaojian Xiao's Sun3D annotation tool. There are some changes I made to work with other dataset, such Georgia Tech and KITTI data-set.

### Please cite the origin Sun3D paper ###


```
@inproceedings{SUN3D,
	author     = {Jianxiong Xiao and Andrew Owens and Antonio Torralba},
	title      = {{SUN3D}: A Database of Big Spaces Reconstructed using {SfM} and Object Labels},
	booktitle  = {ICCV},
	year       = {2013},
}

@inproceedings{SUNRGBD,
	title      = {SUN RGB-D: A RGB-D Scene Understanding Benchmark Suite},
	author     = {Shuran Song and Samuel Lichtenberg and Jianxiong Xiao},
	booktitle  = {CVPR},
	year       = {2015},
}

```

### INSTALL

### RUN

One online annotation tool is deployed on Zhaoyang's machine:

```
http://heracles.cc.gt.atl.ga.us/annotator/player/

```

To specify the data the directory, append CGI input after $name, which is:

```
http://heracles.cc.gt.atl.ga.us/annotator/player/?name=example/&write=true

```
```
http://143.215.131.204/annotator/player/?name=GeorgiaTech3&?width=1384&?height=580&?usemeters=true
```

which means, read the data-set from data/example directory, and enable "write" option.

