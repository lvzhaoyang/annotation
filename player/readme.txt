Please cite the following papers if you use this tool.

Contact with Jianxiong Xiao and Shuran Song if you have questions.

@inproceedings{SUN3D,
	author     = {Jianxiong Xiao and Andrew Owens and Antonio Torralba},
	title      = {{SUN3D}: A Database of Big Spaces Reconstructed using {SfM} and Object Labels},
	booktitle  = {ICCV},
	year       = {2013},
}

@inproceedings{SUNRGBD,
	title      = {SUN RGB-D: A RGB-D Scene Understanding Benchmark Suite},
	author     = {Shuran Song and Samuel Lichtenberg and Jianxiong Xiao},
	booktitle  = {CVPR},
	year       = {2015},
}
