#!/usr/bin/python -tt

# -*- coding: utf-8 -*-

import cgi
import os
import errno
import datetime
import sys
import json
import urllib2

import cgitb
cgitb.enable()
# cgitb.enable(display=0, logdir="/tmp")

import traceback
import getopt
import numbers

from xml.dom.minidom import Document

def parse_element(doc, root, j):
  if isinstance(j, dict):
    for key in j.keys():
      value = j[key]
      if isinstance(value, list):
        for e in value:
          elem = doc.createElement(key)
          parse_element(doc, elem, e)
          root.appendChild(elem)
      else:
        if key.isdigit():
          elem = doc.createElement('item')
          elem.setAttribute('value', key)
        else:
          elem = doc.createElement(key)
        parse_element(doc, elem, value)
        root.appendChild(elem)
  elif isinstance(j, str) or isinstance(j, unicode):
    text = doc.createTextNode(j)
    root.appendChild(text)
  elif isinstance(j, numbers.Number):
    text = doc.createTextNode(str(j))
    root.appendChild(text)
  #else:
  #  raise Exception("bad type '%s' for '%s'" % (type(j), j,))

def parse_doc(root, j):
  doc = Document()
  if root is None:
    if len(j.keys()) > 1:
      raise Exception('Expected one root element, or use --root to set root')
    root = j.keys()[0]
    elem = doc.createElement(root)
    j = j[root]
  else:
    elem = doc.createElement(root)
  parse_element(doc, elem, j)
  doc.appendChild(elem)
  return doc


print "Content-Type: text/xml"     # HTML is following
print                               # blank line, end of headers

form = cgi.FieldStorage()
if "url" not in form:
	sys.stdout.write('error')
else:
	input = urllib2.urlopen(form["url"].value).read()
	j = json.loads(input)
	doc = parse_doc("sun3d", j)
	print doc.toprettyxml(encoding="utf-8", indent="  ")








