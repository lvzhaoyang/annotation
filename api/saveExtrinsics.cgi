#!/usr/bin/python -tt

# -*- coding: utf-8 -*-

import cgi
import os
import errno
import datetime
import sys

import cgitb
cgitb.enable()
# cgitb.enable(display=0, logdir="/tmp")

print "Content-Type: text/html"     # HTML is following
print                               # blank line, end of headers

form = cgi.FieldStorage()
if "data" not in form or "name" not in form:
  sys.stdout.write('wrong parameters')
else:
  # debugging output
  folder = "/n/fs/sun3d/data/" + form["name"].value
  if folder[-1] != "/":
    folder = folder + "/"

  folder = folder + "extrinsics/" + datetime.datetime.now().strftime("%Y%m%d%H%M%S") + ".txt"

  # write the file
  f = open(folder, 'w')
  f.write(form["data"].value)
  f.close()

  os.chmod(folder, 0666)

  sys.stdout.write('success');
