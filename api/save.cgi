#!/usr/bin/python -tt

# -*- coding: utf-8 -*-

import cgi
import os
import errno
import datetime
import sys

import cgitb
cgitb.enable()
#cgitb.enable(display=0, logdir="/tmp")

print "Content-Type: text/html"     # HTML is following
print                               # blank line, end of headers

os.makedirs("/annotator/test_cgi")

cgi.test()

form = cgi.FieldStorage()
if "data" not in form or "name" not in form:
  sys.stdout.write('wrong parameters')
else:
  # debugging output
  folder = "/annotator" + form["name"].value
  if folder[-1] != "/":
    folder = folder + "/"

  if "folder" not in form:
    folder = folder + "annotation/"
  else:
    folder = folder + form["folder"].value
  if folder[-1] != "/":
    folder = folder + "/"

  # check if folder exsit, if not, create a folder
  try:
    os.makedirs(folder)
    os.chmod(folder, 0777)
  except OSError, e:
    if e.errno != errno.EEXIST:
      raise

  filename = 'index.json'

  # check if file exist. if yes, rename the file to back it up
  if os.path.exists(folder+filename):
    # write the file
    f = open(folder+filename+'.tmp', 'w')
    f.write(form["data"].value)
    f.close()
    os.rename(folder+filename,folder+filename+"_"+datetime.datetime.now().strftime('%Y%m%d%H%M%S'))
    os.rename(folder+filename+'.tmp',folder+filename)
  else:
    # write the file
    f = open(folder+filename, 'w')
    f.write(form["data"].value)
    f.close()

  os.chmod(folder+filename, 0666)

  sys.stdout.write('success')
