#!/usr/bin/python -tt

# -*- coding: utf-8 -*-

import cgi
import os
import errno
import datetime
import sys

import cgitb
cgitb.enable()
# cgitb.enable(display=0, logdir="/tmp")

print "Content-Type: text/html"     # HTML is following
print                               # blank line, end of headers

form = cgi.FieldStorage()
if "data" not in form or "name" not in form:
  sys.stdout.write('wrong parameters')
else:
  # debugging output
  folder = "/n/fs/sun3d/data/" + form["name"].value
  if folder[-1] != "/":
    folder = folder + "/"
  folder = folder + "status/"

  #sys.stdout.write(folder)

  # check if folder exsit, if not, create a folder
  try:
    os.makedirs(folder)
    os.chmod(folder, 0777)
  except OSError, e:
    if e.errno != errno.EEXIST:
      raise

  filename = form["filename"].value

  # check if file exist. if yes, rename the file to back it up
  if os.path.exists(folder+filename):
    os.rename(folder+filename,folder+filename+"_"+str(datetime.datetime.now()))

  # write the file
  f = open(folder+filename, 'w')
  f.write(form["data"].value)
  f.close()

  os.chmod(folder+filename, 0777)

  sys.stdout.write('success')
